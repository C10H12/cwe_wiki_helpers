# ruby
# generate wiki page for badge listing

require_relative "data_achievements.rb"

$outputFile = "output_page-#{Time.now.to_i}.txt"

def fillTemplate(title, description, known_reqs)
    filename = title.gsub(" ", "_")
    template = ["===#{title}===" ,
                "''(#{description})''" ,
                "{| class=\"article-table mw-collapsible\" style=\"width:100%;\"" ,
                "|+#{title}" ,
                "!style=\"text-align:center;\"|Image" ,
                "!style=\"text-align:center;\"|Name" ,
                "!style=\"text-align:center;\"|Description" ,
                "!style=\"text-align:center;\"|Requirement" ,
                "!style=\"text-align:center;\"|Reward" ,
                "|-" ,
                "|[[File:Badge_#{filename}_1.png|frameless|128x128px]]" ,
                "|#{title}&nbsp;1" ,
                "| rowspan=\"5\" |#{description}" ,
                "|style=\"text-align:right;\"|#{known_reqs[0]}" ,
                "|style=\"text-align:center;\"|none" ,
                "|-" ,
                "|[[File:Badge_#{filename}_2.png|frameless|128x128px]]" ,
                "|#{title}&nbsp;2" ,
                "|style=\"text-align:right;\"|undocumented" ,
                "|undocumented" ,
                "|-" ,
                "|[[File:Badge_#{filename}_3.png|frameless|128x128px]]" ,
                "|#{title}&nbsp;3" ,
                "|style=\"text-align:right;\"|undocumented" ,
                "|undocumented" ,
                "|-" ,
                "|[[File:Badge_#{filename}_4.png|frameless|128x128px]]" ,
                "|#{title}&nbsp;4" ,
                "|style=\"text-align:right;\"|undocumented" ,
                "|undocumented" ,
                "|-" ,
                "|[[File:Badge_#{filename}_5.png|frameless|128x128px]]" ,
                "|#{title}&nbsp;5" ,
                "|style=\"text-align:center;\"|none" ,
                "|undocumented" ,
                "|}\n\n\n"]
    File.open($outputFile, "a") { |f| f.write(template.join("\n")) }
    puts template if $DEBUG
    return template
end

# generate
34.times do |i|
    requirements = [Badge_req_t1[i], 
                    Badge_req_t2[i], 
                    Badge_req_t3[i], 
                    Badge_req_t4[i], 
                    Badge_req_t5[i]]
    fillTemplate(Badge_titles[i], Badge_desc[i], requirements)
end

puts "saved generated page in #{$outputFile}"
